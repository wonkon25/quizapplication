package com.was.konrad.quiz.frontend;

import lombok.Data;

import static com.was.konrad.quiz.frontend.Difficulty.EASY;

@Data
public class GameOptions {
    private int numberOfQuestions = 5;
    private Difficulty difficulty = EASY;
    private int categoryId = 9;
}

