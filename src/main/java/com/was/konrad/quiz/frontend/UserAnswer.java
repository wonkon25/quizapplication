package com.was.konrad.quiz.frontend;

import lombok.Data;

@Data
public class UserAnswer {
    private String answer;
}
