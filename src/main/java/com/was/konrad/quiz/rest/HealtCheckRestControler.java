package com.was.konrad.quiz.rest;

import com.was.konrad.quiz.dto.HealtCheckDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/health")
public class HealtCheckRestControler {

    @GetMapping
    public HealtCheckDto healthcheck() {
        HealtCheckDto dto = new HealtCheckDto(true, "It`s working!");
        return dto;
    }
}

