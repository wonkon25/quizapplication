<div id="top"></div>

# Quiz Application
> Spring Boot Rest API

<details>
  <summary>Table of Contents</summary>
  <ol>
    <li><a href="#general-information">General Info</a></li>
    <li><a href="#Built With">Built With</a></li>
    <li><a href="#screenshots">Screenshots</a></li>
    <li><a href="#room-for-improvement">Room for Improvement</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>

### General Information
Web version of the quiz game.The user selects the category of questions and their level of difficulty.
On this basis, the program generates a set of tasks for them, saves them and gives the result to the player.

### Built With
* [Java](https://www.oracle.com/pl/java/technologies/javase/jdk11-archive-downloads.html)
* [Spring Framework](https://spring.io/)
* [Spring Boot](https://spring.io/projects/spring-boot)
* [Maven](https://maven.apache.org/)
* [H2 Database](https://www.h2database.com/html/main.html)
* [Thymeleaf](https://www.thymeleaf.org/documentation.html)
* [Bootstrap](https://getbootstrap.com/)

### Screenshots
<details>
  <ol>

![](https://lh3.googleusercontent.com/wX_Pd__K8L5UFEWE_z1e1360lRBXfvXkb0FPXt09NDs6_ftD_60NEeycavBxSOetL0XXnaz60x70l5Rj9C-NrO0--LP-cHrdYuUvZOrgisP-lbVj31P0bB2BK_CVLzwrxcGaAB_vO5iKl-XIwHE__K4ylV2rLuJUg9-u4nqWzoRnH4bYVqhXOHofs9UTUQ4hbEe0uexMw4IkStrhgqrSau-4mi6AbgTJek1QBYBdo_yBCqtxFAlQzPmXBkuvk6A3VZ63UIVlq0oowfyQmWrzfRr6gw-ajLxltUMjzC85P6eTEy-UzNh7OVqw139y5jefoTXoHRLzUIpU0Fi9yclY84PZGPXiIqvRYz3NK6kPNNIOXsz5Xhkj0fjAZ1BzhBd8NAU7PpbBrptHBA9z58M5bLZgAVAG0-JxRUXug01K2fyK5HWferPbzJb9t1amXHkA6PHADyMbUtTKMByy_trE_zOAYjYo2Nn7criWPn3YPnRWd9c5ATNBBZ_4E7r8msxCcb84LytTM8PARwxkFUpk_VKmtSlksUJY4UDqVW2EdWbj6g94XYsWi9S7gN6LiBidKksS3QxeOEQaHQceWryb-UzLqQOBN1UiI_6PL0jLdlVv0G09mmYSBfukvUTvcW7N9hkGiJ9gW5pmPASUUFGNIad3Qh_CwI9X3PHIgf7RUjOX-NbswVSy0ELtF8qC6meUDOoG4ouDenTgux-b3EJ0phulDXoYCFG5kRyJQa2r7_CaLPjFfriyHS9xEVO7gxbY8uTPgVbZPMozePy44SpRrMLb=w600-h385-no?authuser=0)
![](https://lh3.googleusercontent.com/jkzauplfLUpfwvDH7HtNNBEuxBseGP4vGfac6qqkwjxwBScII0YPiVmTQ0f4OYxOjPQSzsbBu6qon6AUQLd2cH3eaeopB_HoMTa3BfmaiOP2VqSM8rlMux4CrO-mmxqcAZGdQkN4_A5QevxQnLKPc7BZs1ohQ4k4wZhKyZ4oV2zRVTsv1qWLiS9P3cKhWBqcATtqIsWawF6K2iIbq7K9OOrSI1UjvPWtZtziQyFne0fuliKU3DSOI8WB3PCJLcpW00y8Nph1LtixOISJEfnLRa9yJlSxRlWkCM3G-MccCsNxOnMHwFMxbxbOZhy0Xecy9yhv6YcVEnLRr9DsLe8uTQXfm1Gj5mD1rQUjBhe2AjCMNkFjLjBHlKRVZVD47uZcgJUP3l9rpCUJc8A9nKQXowCXet7SUcPvhb6SjTPUyBTJhiYM67wlvgo92CxNGwTAPymKhuV43O12Vs6FS3w5WCKHNbZEkzQTwD3w7T0M5TVFbDvTb7LuCrH0kHkzInWmLLfLgDtmedBRqc-FYl98-Vx9zPHnc6-vfN3WtVbOIbUDHz4kCpKOOQNpipS9Rqj4JqDHr1viIrnC9QrXz5rSgk7I1fMUKXFvgYFch-OtnXMovmEhPusiyAuCV1QyPuzfA0C0UtoYjQ1UCO8chLftLBukvAkoAj-n8b47FewIKCW9yGYvhljdsxjMWptK1Xu7H1P8ekGyc0A7o_mRANGeE6ux7u2d_WJb6oYRVbmoUjY0pc93OlLF8Pu-CV0ZEvfNfYZDe8v2DT_KGkGPLk_rfxTF=w600-h382-no?authuser=0)
![](https://lh3.googleusercontent.com/HQpIdeST3S1vXFTKue0s8VCO2lDtqasSyXLK6p6yvb1b0ks_nU6MDq3XZzYIm4w6oQNPNQWHxSrthl1pgfXmrqvKeFp3Pt4tsY48jaNbda8oNFF2hz_-bQKMUF0ysSKWb-VcR3GZSWl_LD56kmxpZ2D5z0jLPmQL4KQcnHIv67Q_yVIzql2NTZFq_vm1OyOIFE9-WzNiE6lUrnaN2yKkOQ-7LWkxpCZFOJkvRKVfF2I0lDvvD96vrMVOnPzwd-iK7G4Vxkmw149saF8pbvV9GabvBd7LDYXxe3WHj0cTn9dD5oGXMCE7uAprlFcP74-wRIdrngsOBRzLDp22om6qFvUwZxsVohm4z4ZClGlyqpBaIzTB8-QD-43yDcRlaf3P_6QbE39nyuwIvMwB4aHSVTeM3nGt8OvDJZsghMUmrAYXFVuuaaZqAuRzhGmiULdSjKXu7ERD8Xj9WjW8QO0iqskK37rRblFl9ZCmaGhkvXPf-FiwZ7mHKLOWauZjL_LlTVHHyIVzDT_cIFfbSmnzbPdIME5S_pplGVe7foTNmOWeyiUs6Ylhhod9pc9LVNpjkvbQOYhN8BVCuTnJVBHm8jtd6B5AWra44_3eqLfIOdWg1YhcuZtOQD4PvwAsluAZ5ypFrFHRLnDgA4Est2Xn5_QV485aYUIhOCSajBKX2KblyJP68rU2fzP8WA61K88Ibtt7w66e_Xi9oW_RUMsGV12tIqLKOfZtCAKXIytOX2Bjpnogsha00AjfHNj0UvOgImKlupSJYj5ugdR42Nb1mIzR=w600-h362-no?authuser=0)
![](https://lh3.googleusercontent.com/4-7wu6W--bwdBYso_seKayCa62ValU5gVVSQ1FqlidyNLUI1SmrqWKNym9ppB6zCaLiq7wHXO2iq9OPlv6FjZQ5ETibDdq3omYmxzz5I6X-s91pSduVo4FuU0tsDueDkZZG1wPWPt9rv4voazieziojUk9H2FnW34-MaS7og2M3jo40JXSxURnB9JejhEFMe_dS5AhFFn20pD1HoXQ_1is6dTPA-5k3E__2KwpwIgRUQTKESbXVTc5PJRGC5DTtu-plsWx_fKJ5Vm_MSKvyOzUCnldKbnuFmROVmjNXUMUCbQAfhzh66OSBKwxAMCd0EiPMMT-MUzAeVYIMF9f5We92EMkTHYGVFhnUHUtzCN7x-qS3_9z3yVtmDL0CYYbz7044O4bDZ2V2O-tAN06Tk-ELCodRVsyIkPCTJdKx_miFRrKa2kHVdOhkN76K3KSdoZBTbH_xCYF8fLF2nQJC9phgncWytmglcBE35VUNnSkOqULaCKF7xIcnA6265O7PITaJ8qnphPfkwDL9FZ-yOiQ_hexfAaHFKya4aVMBywzu-l0Jz5KGfXAigfthllbbs3Obal37AyQ67sQsClFeK9RwzvztvywAbB7cQE6PK2sT06YrN3ncl6VXauluCWmxCmar_cskZngU3gnmzWlnaQnFY0SVdrI5Tdjp9pFC8g4z5WoTHGocOkcKi3KX25UaINrjmMQBUY8Qw2gT1glH27HhXDbIQCq1T-UNSEpenFdqJmp8Txl1IvXz0jM9ChNVdsfBEAe2X9uj8e1Vmci8fJV-w=w600-h330-no?authuser=0)
  </ol>
</details>

### Room for Improvement
- Saving the best scores of players
- Improving the appearance of the website

To do:
- Cache for QuizDataService
- Time constraints in answering

### Acknowledgements

- This project was inspired by  [Open Trivia Database](https://opentdb.com/)
<p align="right">(<a href="#top">back to top</a>)</p>
